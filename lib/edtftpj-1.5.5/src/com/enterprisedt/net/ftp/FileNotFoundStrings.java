/**
 *
 *  Copyright (C) 2000-2007  Enterprise Distributed Technologies Ltd
 *
 *  www.enterprisedt.com
 *
 *  Change Log:
 *
 *        $Log: FileNotFoundStrings.java,v $
 *        Revision 1.1  2007/01/12 02:04:23  bruceb
 *        string matchers
 *
 *
 */
package com.enterprisedt.net.ftp;

/**
 *  Contains fragments of server replies that indicate no files were
 *  found in a supplied directory.
 *
 *  @author      Bruce Blackshaw
 *  @version     $Revision: 1.1 $
 */
final public class FileNotFoundStrings extends ServerStrings {

    /**
     * Server string indicating file not found
     */
    final public static String FILE_NOT_FOUND = "NOT FOUND";
    
    /**
     * Server string indicating file not found
     */
    final public static String NO_SUCH_FILE = "NO SUCH FILE";
    
    /**
     * Constructor. Adds the fragments to match on
     */
    public FileNotFoundStrings() {
        add(FILE_NOT_FOUND);
        add(NO_SUCH_FILE);
    }

}
