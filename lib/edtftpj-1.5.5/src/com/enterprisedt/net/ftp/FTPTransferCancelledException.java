/**
 *
 *  Java FTP client library.
 *
 *  Copyright (C) 2000-2003 Enterprise Distributed Technologies Ltd
 *
 *  www.enterprisedt.com
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Bug fixes, suggestions and comments should be should posted on 
 *  http://www.enterprisedt.com/forums/index.php
 *
 *  Change Log:
 *
 *    $Log: FTPTransferCancelledException.java,v $
 *    Revision 1.3  2006/10/11 08:58:29  hans
 *    made cvsId final
 *
 *    Revision 1.2  2006/02/16 19:48:48  hans
 *    Fixed comment
 *
 *    Revision 1.1  2005/11/10 19:44:00  bruceb
 *    for when transfers are cancelled
 *
 *    Revision 1.6  2005/06/03 11:26:25  bruceb
 *    comment change
 *
 *    Revision 1.5  2004/07/23 08:27:43  bruceb
 *    new constructor
 *
 *    Revision 1.4  2002/11/19 22:01:25  bruceb
 *    changes for 1.2
 *
 *    Revision 1.3  2001/10/09 20:54:08  bruceb
 *    No change
 *
 *    Revision 1.1  2001/10/05 14:42:04  bruceb
 *    moved from old project
 *
 */

package com.enterprisedt.net.ftp;

/**
 *  Thrown when an FTP transfer has been cancelled
 *
 *  @author     Bruce Blackshaw
 *  @version    $Revision: 1.3 $
 *
 */
 public class FTPTransferCancelledException extends FTPException {

    /**
     *  Revision control id
     */
    public static final String cvsId = "@(#)$Id: FTPTransferCancelledException.java,v 1.3 2006/10/11 08:58:29 hans Exp $";

    /**
     * Serial uid
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * Message
     */
    private static final String MESSAGE = "Transfer was cancelled";

    /**
     *   Constructor. Delegates to super.
     */
    public FTPTransferCancelledException() {
        super(MESSAGE);
    }
}
