/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package opsftp;

import com.enterprisedt.net.ftp.FTPMessageListener;
import javax.swing.JLabel;
import javax.swing.JTextPane;

/**
 *
 * @author i.cornish
 */
public class MessageListener implements FTPMessageListener {
    
    private JTextPane lbl = null;

    public void logCommand(String command) {
        //System.out.println("[]<--\t"+command);
        //progressText_Label1.setText( newProgresstext2 );
    }

    public void setDisplayComp(Object obj){
        lbl = (JTextPane)obj;
    }

    public void logReply(java.lang.String reply) {
        try{

            String txt = lbl.getText();
            lbl.setText(txt+"\n[]-->\t"+reply);
            lbl.getCaret().setDot(lbl.getText().length());
        }catch(NullPointerException npe){
            //System.out.println("Could not get reference to label object");
        }
        
        //System.out.println("[]-->\t"+reply);
    }
}
