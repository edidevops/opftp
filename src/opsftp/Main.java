package opsftp;

import com.edi.common.MySqlConnection.MySqlConnection;
import com.enterprisedt.net.ftp.FTPClient;
import com.enterprisedt.net.ftp.FTPException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import javax.swing.JOptionPane;

/**
 *
 * @author i.cornish
 *
 *
 */
public class Main extends FTPClient {

    private MySqlConnection	mysql;
    private Connection	conn;
    private Statement stmt;
    private String site = "";
    private boolean crccheck = false;
    private String[] serverconf;
    private boolean wasError = false;
    private boolean CRCError = false;
    private boolean updown = true;
    private String fileext = "";
    private boolean filefilter = false;
    /* the following vars should be populated using
     * the FTPServer.cfg configuration file, base on
     * the site parameter passed in the command line
     */
    private String host = "";
    private String user = "";
    private String pass = "";
    private int port = 21;

    /* the following vars should be populated by the
     * praseCommandLine
     */
    private String logfile = "o:\\AUTO\\logs\\OpsFtp.log";
    private String localpath = "o:\\AUTO\\Configs\\";
    private String remotepath = "/idctest/";
    private progress prog = new progress();
    private String server = "";
    private String database = "";
    private boolean insert = false;
    //private progress prog2 = new progress();


    /* public construction method */
    public Main(String[] args) {

        praseCommandLine(args);

        prog.setTitle(this.host);

        MessageListener msgl1 = new MessageListener() ;
        msgl1.setDisplayComp(prog.progressText_Label1);
        this.setMessageListener( msgl1 );

        this.setProgressMonitor( new ProgressMonitor() );

        // Display the progress window
        //prog2.setVisible(true);
        //prog2.setProgressText("Setting up, Please wait...");
        prog.setVisible(true);
        String command="";
        prog.setProgressText2("[]<--\t"+command);
        prog.setProgressText("Setting up, Please wait...");
        prog.progressText_Label1.setText("[]<--\t"+this.host);
        this.setTransferBufferSize(10);


        if (updown == false) {
            getRemoteFiles();
        } else {
            try {
                // set up connection details and connect to server
                prog.setProgressText("Connecting to remote FTP server");
                setRemoteHost(host);
                setConnectMode(com.enterprisedt.net.ftp.FTPConnectMode.ACTIVE);
                connect();

                // Login to remote FTP server
                login(user, pass);

                // change directory.
                chdir(remotepath);

                // Get the LOCAL directory file list for the path
                // specified in the command line arguments.
                File checkLocalPath = new File(localpath);
                if (!checkLocalPath.exists()) {
                    if(insert)InsertintoDB(server,database,"",localpath,remotepath,"fail","The local folder '" + localpath + "' does not exist");
                    appendLog("The local folder '" + localpath + "' does not exist");
                    System.exit(1);
                }
                prog.setProgressText("Building upload file list for " + localpath + ", Please wait...");
                File[] localFiles = new File(localpath).listFiles();

                // Loop thought each of the file in the localFile array
                for (int i = 0; i < localFiles.length; i++) {

                    // if the element within localFiles[] is not
                    // a directory and is a file then we will process
                    // the item as a file.
                    if (!localFiles[i].isDirectory() && localFiles[i].isFile()) {

                        prog.setProgressText("Checking if file nedded uploading...");
                        boolean wasFileUploaded = false, chkFileUploaded = false;

                        String t = localFiles[i].toString();
                        t = t.substring(this.localpath.length());

                        // if the filefilter is true then we need to check if the file
                        // matches the filefilter parameter before uploading.
                        if (filefilter==true) {
                            if (localFiles[i].toString().toUpperCase().endsWith(fileext.toUpperCase())) {
                                prog.setProgressText("Uploading '" + localFiles[i].toString() + "'");
                                if (getFileType(localFiles[i].toString())){
                                     setType(transferType.BINARY);
                                }else{
                                     setType(transferType.ASCII);
                                }
                                put(localFiles[i].toString(), remotepath + t,false);
                                wasFileUploaded = true;
                                chkFileUploaded = true;
                            }else{
                                wasFileUploaded = false;
                                chkFileUploaded = true;
                            }
                        }else{
                            prog.setProgressText("Uploading '" + localFiles[i].toString() + "'");
                            if (getFileType(localFiles[i].toString())){
                                 setType(transferType.BINARY);
                            }else{
                                 setType(transferType.ASCII);
                            }
                            put(localFiles[i].toString(), remotepath + t,false);
                            wasFileUploaded = true;
                            chkFileUploaded = true;
                        }

                        //System.out.println(localFiles[i].toString().substring(localFiles[i].toString().lastIndexOf("\\")+1,localFiles[i].toString().length()));
                        String fname = localFiles[i].toString().substring(localFiles[i].toString().lastIndexOf("\\")+1,localFiles[i].toString().length());
                        // Do we need to check the CRC values of the two files?
                        CRCError = false;
                        if (this.crccheck == true) {
                            if (wasFileUploaded == true) {
                                // Get the Local and Remote CRC values
                                prog.setProgressText("Validating uploaded file...");                                
                                String localCRC = getCRC( localFiles[i] );
                                String remoteCRC = quote("XCRC " + t + "", null);                                
                                //System.out.println(remoteCRC);
                                // Convert the localCRC value from Long to Hex
                                Long l = Long.parseLong(localCRC);
                                localCRC = Long.toHexString(l).toUpperCase();                                
                                //System.out.println(localCRC);
                                // Compare the two CRC values
                                
                                if (localCRC.compareToIgnoreCase(remoteCRC) != 0) {
                                    prog.setProgressText("Uploaded file '" + localFiles[i].toString() + "' is invalid");
                                    if(insert)InsertintoDB(server,database,fname,localpath,remotepath,"fail","CRC Failure: " + localFiles[i].toString());
                                    appendLog("CRC Failure: Site--> " + site + ", LocalPath--> " + localpath + "/" + localFiles[i].toString() + ", RemotePath--> " + remotepath);
                                    wasError = true;
                                    CRCError = true;
                                }else{
                                    if(insert)InsertintoDB(server,database,fname,localpath,remotepath,"successful","");
                                    CRCError = false;
                                    wasError = false;
                                }
                            }else{
                                wasError = true;
                                if(!chkFileUploaded){
                                    if(insert)InsertintoDB(server,database,fname,localpath,remotepath,"fail","File could not uploaded '" + localFiles[i].toString());
                                    appendLog("File could not uploaded Site--> " + site + ", LocalPath--> " + localpath + "/" + localFiles[i].toString() + ", RemotePath--> " + remotepath);
                                }
                            }
                        }

                    }
                }

                // Disconnect from remote FTP server
                prog.setProgressText("Disconnecting from FTP Server...");
                quit();

                // display finished message and wait for 3 seconds
                prog.setProgressText("FINISHED!");
                Thread.sleep(3000);

                // hide the window
                prog.setVisible(false);
                if(insert) closeDB();
                // exit with a clean exit code (0)
                if (wasError == true && CRCError == true) {
                    System.exit(2);
                }else if (wasError == true && CRCError == false) {
                    System.exit(1);
                } else {
                    System.exit(0);
                }

            } catch (InterruptedException ex) {
                //System.out.println(ex.getMessage());
                if(insert)InsertintoDB(server,database,"",localpath,remotepath,"fail","InterupedException: " + ex.getMessage());
                appendLog("InterupedException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localpath + ", RemotePath--> " + remotepath);
                System.exit(1);
            } catch (IOException ex) {
                //System.out.println(ex.getMessage());
                if(insert)InsertintoDB(server,database,"",localpath,remotepath,"fail","IOException: " + ex.getMessage());
                appendLog("IOException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localpath + ", RemotePath--> " + remotepath);
                System.exit(1);
            } catch (FTPException ex) {
                //System.out.println(ex.getMessage());
                if(insert)InsertintoDB(server,database,"",localpath,remotepath,"fail","FTPException: " + ex.getMessage());
                appendLog("FTPException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localpath + ", RemotePath--> " + remotepath);
                System.exit(1);
            }catch (Exception ex) {
                //System.out.println(ex.getMessage());
                if(insert)InsertintoDB(server,database,"",localpath,remotepath,"fail","Exception: " + ex.getMessage());
                appendLog("Exception: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localpath + ", RemotePath--> " + remotepath);
                System.exit(1);
            }
        }
    }

    private void getRemoteFiles() {
        try {
            wasError = false;
            prog.setVisible(true);
            prog.setProgressText("Setting up, Please wait...");

            // set up connection details and connect to server
            prog.setProgressText("Connecting to remote FTP server");
            setRemoteHost(host);
            setConnectMode(com.enterprisedt.net.ftp.FTPConnectMode.ACTIVE);
            connect();

            // Login to remote FTP server
            login(user, pass);



            // Get remote directory listing
            String[] remoteDirList = dir(remotepath);

            for (int i = 0; i < remoteDirList.length; i++) {
                //get(localpath+remoteDirList[i] , remotepath+remoteDirList[i] );



                File localFile = new File(localpath + remoteDirList[i]);
                String t = localFile.toString();

                boolean doCrc = false;

                t = t.substring(this.localpath.length());
                if (filefilter) {
                    if (remoteDirList[i].toUpperCase().endsWith(fileext.toUpperCase())) {
                        if (getFileType(remotepath + remoteDirList[i])){
                                     setType(transferType.BINARY);
                                }else{
                                     setType(transferType.ASCII);
                                }
                        get(localpath + remoteDirList[i], remotepath + remoteDirList[i]);
                        doCrc = true;
                    }
                } else {
                    get(localpath + remoteDirList[i], remotepath + remoteDirList[i]);
                    doCrc = true;
                }

                // Do we need tocheck the CRC values of the two files?
                CRCError = false;
                if (this.crccheck == true) {
                    if (doCrc) {
                        // get Local and Remote CRC vlaue
                        prog.setProgressText("Validating downloaded file...");
                        String localCRC = getCRC(localFile);
                        String remoteCRC = quote("XCRC \"" + remotepath + remoteDirList[i] + "\"", null);

                        //System.out.println (localCRC);

                        // do number conversion
                        Long l = Long.parseLong(localCRC);
                        localCRC = Long.toHexString(l);

                        //System.out.println( "L:"+Long.toHexString(l) );
                        //System.out.println( "R:"+remoteCRC);


                        // Compare localCRC.compareToIgnoreCase(l remoteCRC) != 0 ){the localCRC and the Remote CRC
                        if (localCRC.compareToIgnoreCase(remoteCRC) != 0) {
                            prog.setProgressText("Downloaded file '" + localFile.toString() + "' is invalid");
                            if(insert)InsertintoDB(server,database,localFile.toString(),localpath,remotepath,"fail","CRC Failure: " + localFile.toString());
                            appendLog("CRC Failure: Site--> " + site + ", LocalPath--> " + localpath + "/" + localFile.toString() + ", RemotePath--> " + remotepath);
                            wasError = true;
                            CRCError = true;
                        }else{
                            if(insert)InsertintoDB(server,database,localFile.toString(),localpath,remotepath,"successful","");
                            CRCError = false;
                            wasError = false;
                        }
                    }
                }

            }

            if(insert)closeDB();
            if (wasError == true && CRCError == true) {
                prog.setVisible(false);
                System.exit(2);
            }else if (wasError == true && CRCError == false) {
                prog.setVisible(false);
                System.exit(1);
            } else {
                prog.setVisible(false);
                System.exit(0);
            }
        } catch (IOException ex) {
            //System.out.println(ex.getMessage());
            if(insert)InsertintoDB(server,database,"",localpath,remotepath,"fail","IOException: " + ex.getMessage());
            appendLog("IOException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localpath + ", RemotePath--> " + remotepath);
            System.exit(1);
        } catch (FTPException ex) {
            //System.out.println(ex.getMessage());
            if(insert)InsertintoDB(server,database,"",localpath,remotepath,"fail","FTPException: " + ex.getMessage());
            appendLog("FTPException: " + ex.getMessage() + "Site--> " + site + ", LocalPath--> " + localpath + ", RemotePath--> " + remotepath);
            System.exit(1);
        }
    }

    private void praseCommandLine(String[] args) {
        if (args.length == 0) {
            appendLog("Parameters not found");
        }

        int chk = 0;
        for (int i = 0; i < args.length; i++) {

            if (args[i].compareToIgnoreCase("-site") == 0) {
                this.site = args[i + 1];

                // read ftp site config
                loadFTPConfig();
                for (int x = 1; x < serverconf.length; x++) {
                    if (serverconf[x].startsWith(this.site.toUpperCase())) {
                        // do something here
                        //appendLog("Found the site configuration line in FTPServer.cfg");

                        String temp[] = serverconf[x].split("\t");
                        this.host = temp[1];
                        this.user = temp[2];
                        this.pass = temp[3];
                        break;
                    }
                }
            }

            if (args[i].compareToIgnoreCase("-local") == 0) {
                this.localpath = args[i + 1];
            }

            if (args[i].compareToIgnoreCase("-upload") == 0) {
                this.updown = true;
            }

            if (args[i].compareToIgnoreCase("-download") == 0) {
                this.updown = false;
            }

            if (args[i].compareToIgnoreCase("-remote") == 0) {
                this.remotepath = args[i + 1];
            }

            if (args[i].compareToIgnoreCase("-fileext") == 0) {
                this.filefilter = true;
                this.fileext = args[i + 1];
            }

            if (args[i].compareToIgnoreCase("-svr") == 0) {
                this.server = args[i + 1];
                chk++;
            }

            if (args[i].compareToIgnoreCase("-db") == 0) {
                this.database = args[i + 1];
                chk++;
            }            

            if (args[i].compareToIgnoreCase("-XCRC") == 0) {
                if (args[i + 1].compareToIgnoreCase("ON") == 0) {
                    this.crccheck = true;
                } else {
                    this.crccheck = false;
                }
            }

            if (args[i].compareToIgnoreCase("-port") == 0) {
                try {
                    this.port = Integer.parseInt(args[i + 1]);
                } catch (Exception ex) {
                    //ex.printStackTrace();
                    System.exit(1);
                }
            }

            if (args[i].compareToIgnoreCase("-log") == 0) {
                try {
                    this.logfile = args[i + 1];
                } catch (Exception ex) {
                    //ex.printStackTrace();
                    System.exit(1);
                }
            }
            
        }
        
        try{
            //checking for db entry.
            if(chk == 2){
                insert = true;
                String [] condetail = GetConnDetail(server);
                mysql = new MySqlConnection(condetail[0], condetail[1], database, condetail[2]);
                conn = mysql.getConnection();
                stmt = conn.createStatement();
            }else{
                insert = false;
            }
        }catch(Exception e){
            //appendLog("" e.getMessage());
        }
    }

    /* load FTPServer config */
    private void loadFTPConfig() {
        try {
            BufferedReader in = new BufferedReader(new FileReader("O:\\AUTO\\Configs\\FTPServers.cfg"));
            String str;
            int linecount = 0;
            while ((str = in.readLine()) != null) {
                linecount++;
                //serverconf[xxx] = str;

            }
            serverconf = new String[linecount + 1];
            in.close();

        } catch (IOException e) {
        }

        try {
            BufferedReader in = new BufferedReader(new FileReader("O:\\AUTO\\Configs\\FTPServers.cfg"));
            String str;
            int currentline = 0;
            while ((str = in.readLine()) != null) {
                currentline++;
                serverconf[currentline] = str;
            }
            in.close();

        } catch (IOException e) {
        }


    }

    /* public method for producing a crc32 on a local file check */
    private String getCRC(File fileName) {
        try {
            CheckedInputStream cis = null;
            //long filesize = 0;
            // Computer CRC32 checksum
            cis = new CheckedInputStream(new FileInputStream(fileName.toString()), new CRC32() );
            //filesize = fileName.length();            

            byte[] buf = new byte[128];
            while ( cis.read(buf)>=0) {
            }
            
            long checksum = cis.getChecksum().getValue();
           // System.out.println(checksum + " " + filesize + " " + fileName);

            return String.valueOf(checksum);

        } catch (Exception e) {
            appendLog("getCRC: " + e.getMessage());
            //e.printStackTrace();
        }
        return null;
    }

    /* private method to get the crc code from ftp server */
    private String remoteCRC(String filepath) {
        try {
            return quote("XCRC " + filepath, null);
        } catch (IOException ex) {
        } catch (FTPException ex) {
        }
        return null;
    }

    /* append a message to the OPSFTP log file */
    private void appendLog(String logMessage) {
        try {
            // Build DateTime String
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    java.util.Date now = new java.util.Date();
	    String sDate= sdf.format(now);

            BufferedWriter out = new BufferedWriter(new FileWriter(logfile, true));
            out.write(sDate + "\t" + logMessage + "\r\n");
            out.close();
        } catch (IOException e) {
        }
    }
private boolean getFileType(String fname){
    fname = fname.toLowerCase();
    if (fname.endsWith(".zip"))
        return true;
    if (fname.endsWith(".exe"))
        return true;
    if (fname.endsWith(".pdf"))
        return true;
    if (fname.endsWith(".z01"))
        return true;
    if (fname.endsWith(".z02"))
        return true;
    if (fname.endsWith(".z03"))
        return true;
    if (fname.endsWith(".tar"))
        return true;
    if (fname.endsWith(".gz"))
        return true;
    if (fname.endsWith(".xls"))
        return true;

    //check for extension between 0 to 999
    try{
        String [] tmpext = fname.split("\\.");
        if(Integer.parseInt(tmpext[tmpext.length-1]) > 0 && Integer.parseInt(tmpext[tmpext.length-1]) <= 999){
            return true;
        }
    }catch(Exception e){
    }

    return false;
}

private void InsertintoDB(String svr,String db,String fname,String lfld,String rfld,String trns,String cmt){
    try{        
        String sql = "Insert into " + db + "." + "fileupload(filename, site, localfolder, remotefolder, " +
                           "transfer, comment, acttime)" +
                           "values('" + fname + "'," + "'" + site + "'," + "'" + lfld + "'," + "'" + rfld + "'," +
                                   "'" + trns + "'," + "'" + cmt + "', now())";
        int cnt = stmt.executeUpdate(sql);
    }catch(Exception e){
        appendLog("InsertintoDB: " + e.getMessage());
    }
}

private void closeDB(){
    try{
        stmt.close();
        conn = null;
        mysql = null;
    }catch(Exception e){
        appendLog("closeDB: " + e.getMessage());
    }
}

private String [] GetConnDetail(String svr){
      try{
          String [] detail = new String[3];
          String [] tvalues = null;
           File sourcefile = new File("O:\\AUTO\\Configs\\DbServers.cfg");
            if(sourcefile.exists()){
                BufferedReader in = new BufferedReader(new FileReader(sourcefile));
                String str = null;
                while ((str = in.readLine()) != null) {
                    //if(str.indexOf(svr)>=0)
                    tvalues = str.split("\t");
                    if(tvalues[0].compareToIgnoreCase(svr)==0){
                        detail[0] = tvalues[2];
                        detail[1] = tvalues[3];
                        detail[2] = tvalues[4];
                    }
                }
                in.close();
            } else{
                appendLog("Could not found the config file: O:\\AUTO\\Configs\\DbServers.cfg");
            }

          return detail;
      }catch(Exception e){
            appendLog("GetConnDetail: " + e.getMessage());
            return null;
      }
  }

    /* public bootstrap method, this is the java VM entry point */
    public static void main(String[] args) {
        new Main(args);
    }
}
